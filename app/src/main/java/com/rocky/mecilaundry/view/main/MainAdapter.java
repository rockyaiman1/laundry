package com.rocky.mecilaundry.view.main;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rocky.mecilaundry.R;
import com.rocky.mecilaundry.model.nearby.ModelResults;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {

    ArrayList<ModelResults> modelResultsArrayList = new ArrayList<>();
    Context context;

    public MainAdapter(Context context){
        this.context = context;
    }

    public void setLocationAdapter(ArrayList<ModelResults> items){
        modelResultsArrayList.clear();
        modelResultsArrayList.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rekomendasi, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
        ModelResults modelResults = modelResultsArrayList.get(position);

        //set rating
        float newValue = (float) modelResults.getRating();
        holder.ratingBar.setNumStars(5);
        holder.ratingBar.setStepSize((float) 0.5);
        holder.ratingBar.setRating(newValue);

        holder.tvNamaJalan.setText(modelResults.getVicinity());
        holder.tvNamaLokasi.setText(modelResults.getName());
        holder.tvRating.setText("("+ modelResults.getRating()+")");

        //set data to share & intent
        String strNamaLokasi = modelResultsArrayList.get(position).getName();
        double strLat = modelResultsArrayList.get(position).getModelGeometry().getModelLocation().getLat();
        double strLong = modelResultsArrayList.get(position).getModelGeometry().getModelLocation().getLng();

        //intent to share location
        holder.imageShare.setOnClickListener(v -> {
            String strUri = "https://maps.google.com/maps?saddr=" + strLat + "," + strLong;
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, strNamaLokasi);
            intent.putExtra(Intent.EXTRA_TEXT, strUri);
            context.startActivity(Intent.createChooser(intent, "Bagikan :"));
        });

    }

    @Override
    public int getItemCount() {
        return modelResultsArrayList.size();
    }

    public static class MainViewHolder extends RecyclerView.ViewHolder{

        LinearLayout linearRute;
        TextView tvNamaJalan, tvNamaLokasi, tvRating;
        ImageView imageShare;
        RatingBar ratingBar;

        public MainViewHolder(View itemView){
            super(itemView);
            linearRute = itemView.findViewById(R.id.linearRute);
            tvNamaJalan = itemView.findViewById(R.id.tvNamaJalan);
            tvNamaLokasi = itemView.findViewById(R.id.tvNamaLokasi);
            tvRating = itemView.findViewById(R.id.tvRating);
            imageShare = itemView.findViewById(R.id.imageShare);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }
    }
}
