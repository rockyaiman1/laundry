package com.rocky.mecilaundry.view.main;

public class ModelMenu {

    String tvTittle;
    int imageDrawable;

    public ModelMenu(String tvTittle, int imageDrawable) {
        this.tvTittle = tvTittle;
        this.imageDrawable = imageDrawable;
    }

    public String getTvTittle() {
        return tvTittle;
    }

    public void setTvTittle(String tvTittle) {
        this.tvTittle = tvTittle;
    }

    public int getImageDrawable() {
        return imageDrawable;
    }

    public void setImageDrawable(int imageDrawable) {
        this.imageDrawable = imageDrawable;
    }
}
