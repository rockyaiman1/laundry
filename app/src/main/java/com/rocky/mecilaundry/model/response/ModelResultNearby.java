package com.rocky.mecilaundry.model.response;

import com.google.gson.annotations.SerializedName;
import com.rocky.mecilaundry.model.nearby.ModelResults;

import java.util.List;

public class ModelResultNearby {

    @SerializedName("results")
    private List<ModelResults> modelResults;

    public List<ModelResults> getModelResults(){
        return modelResults;
    }

    public void setModelResults(List<ModelResults> modelResults) {
        this.modelResults = modelResults;
    }
}
