package com.rocky.mecilaundry.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.rocky.mecilaundry.model.nearby.ModelResults;
import com.rocky.mecilaundry.model.response.ModelResultNearby;
import com.rocky.mecilaundry.networking.ApiClient;
import com.rocky.mecilaundry.networking.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {

    MutableLiveData<ArrayList<ModelResults>> modelResultsMutableLiveData = new MutableLiveData<>();
    public static String strApiKey = "AIzaSyDWKn6Rbei43bT_IaqW0GdvVfCNTZyAXTM";

    public void SetMarkerLocation(String strLocation){
        ApiInterface apiServie = ApiClient.getClient().create(ApiInterface.class);

        Call<ModelResultNearby> call = apiServie.getDataResult(strApiKey,"Laundry", strLocation,"distance");
        call.enqueue(new Callback<ModelResultNearby>() {
            @Override
            public void onResponse(Call<ModelResultNearby> call, Response<ModelResultNearby> response) {
                if (!response.isSuccessful()){
                    Log.e("response", response.toString());
                }else if (response.body() != null){
                    ArrayList<ModelResults> items = new ArrayList<>(response.body().getModelResults());
                    modelResultsMutableLiveData.postValue(items);
                }
            }

            @Override
            public void onFailure(Call<ModelResultNearby> call, Throwable t) {
                Log.e("failure", t.toString());
            }
        });
    }

    public LiveData<ArrayList<ModelResults>> getMarkerLocation(){
        return modelResultsMutableLiveData;
    }
}
