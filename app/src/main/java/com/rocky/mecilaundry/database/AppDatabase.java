package com.rocky.mecilaundry.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.rocky.mecilaundry.database.dao.LaundryDao;
import com.rocky.mecilaundry.model.ModelLaundry;

@Database(entities = {ModelLaundry.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract LaundryDao laundryDao();
}
